# How to test codeml

Codeml by default reads a file called `codeml.ctl`.
* Use a fixed topology: `runmode = 0`
* Specify the tree file in newick format: `treefile = filename`
* Fix the branch lengths: `fix_blength =1`
* To select branch-site models: `model = 2`
* To select fe, f1x4, f3x4, F61, mg94, mg94x3, fMutSel0, fMutSel: `CodonFreq = num`
* To set the parameters, make a file called `in.codeml`.

Codeml reads parameters from a file called `in.codeml`.
* in codeml.c:
  * `main`: `finitials = fopen("in.codeml", "r")`
  * `GetInitials(x,fromfile)`: `readx(x, fromfile)`
* in`src/treesub.c`:
  * 'readx( )'

```
// CodonFreq
enum { Fequal, F1x4, F3x4, Fcodon, F1x4MG, F3x4MG, FMutSel0, FMutSel } CodonFreqs;

enum { NSbranchB = 1, NSbranch2, NSbranch3 } NSBranchModels;

// NSsites
enum {
   NSnneutral = 1, NSpselection, NSdiscrete, NSfreqs, NSgamma, NS2gamma,
   NSbeta, NSbetaw, NSbetagamma, NSbeta1gamma, NSbeta1normal, NS02normal,
   NS3normal, NSM2aRel = 22
} NSsitesModels;
```
* com.np  = number of parameters
* com.npi = number of frequency parameters
* com.codonf = CodonFreq parameter (Fequal, F1x4, F3x4, Fcodon, F1x4MG, F3x4MG, FMutSel0, FMutSe


# GetInitialsCodon

starting at com.ntime + com.nrgene
* com.ntime: ??
* com.nrgene: ??
* 1: kappa

* FMutSel0
  * 3: pi_TCA
  * 19: amino acid fitnesses
* FMutSel
  * 3: pi_TCA
  * num_codons-1: codon fitnesses
* Fcodon
  * num_codons-1: codon fitnesses
* F1x4 or F1x4MG
  * 3: pi_TCA
* F3x4 or F3x4MG
  * 3: pi_TCA
  * 3: pi_TCA
  * 3: pi_TCA



# Example

```
Codon position x base (3x4) table, overall

position  1:    T:0.10553    C:0.23501    A:0.28523    G:0.37423
position  2:    T:0.31765    C:0.25514    A:0.30727    G:0.11994
position  3:    T:0.21975    C:0.41004    A:0.04344    G:0.32676
Average         T:0.21431    C:0.30006    A:0.21198    G:0.27365

TREE #  1:  ((((1, 2), (3, 4)), ((5, 6), (7, 8))), ((9, 10), (11, 12)), ((13, 14), (15, 16)));   MP score: -1
lnL(ntime: 29  np: 34):  -7702.381669      +0.000000
  17..18   18..19   19..20   20..1    20..2    19..21   21..3    21..4    18..22   22..23   23..5    23..6    22..24   24..7    24..8    17..25   25..26   26..9    26..10   25..27   27..11   27..12   17..28   28..29   29..13   29..14   28..30   30..15   30..16 
 0.364570 0.189668 0.158456 0.211749 0.171810 0.257082 0.250085 0.181791 0.227695 0.265948 0.225991 0.180965 0.105908 0.193219 0.241487 0.167273 0.216258 0.172932 0.227773 0.216140 0.249109 0.140416 0.108726 0.196559 0.216064 0.188541 0.221887 0.203868 0.203608 3.910082 0.446136 0.526033 0.139216 15.093704

Note: Branch length is defined as number of nucleotide substitutions per codon (not per neucleotide site).

tree length =  5.955581

((((1: 0.211749, 2: 0.171810): 0.158456, (3: 0.250085, 4: 0.181791): 0.257082): 0.189668, ((5: 0.225991, 6: 0.180965): 0.265948, (7: 0.193219, 8: 0.241487): 0.105908): 0.227695): 0.364570, ((9: 0.172932, 10: 0.227773): 0.216258, (11: 0.249109, 12: 0.140416): 0.216140): 0.167273, ((13: 0.216064, 14: 0.188541): 0.196559, (15: 0.203868, 16: 0.203608): 0.221887): 0.108726);

((((A1: 0.211749, B1: 0.171810): 0.158456, (C1: 0.250085, D1: 0.181791): 0.257082): 0.189668, ((E1: 0.225991, F1: 0.180965): 0.265948, (G1: 0.193219, H1: 0.241487): 0.105908): 0.227695): 0.364570, ((A2: 0.172932, B2: 0.227773): 0.216258, (C2: 0.249109, D2: 0.140416): 0.216140): 0.167273, ((E2: 0.216064, F2: 0.188541): 0.196559, (G2: 0.203868, H2: 0.203608): 0.221887): 0.108726);

Detailed output identifying parameters

kappa (ts/tv) =  3.91008


MLEs of dN/dS (w) for site classes (K=4)

site class             0        1       2a       2b
proportion       0.44614  0.52603  0.01277  0.01506
background w     0.13922  1.00000  0.13922  1.00000
foreground w     0.13922  1.00000 15.09370 15.09370
```

OK, so it looks like there are
* some branch lengths
* kappa = 3.910082
* p0 = 0.446136
* p1 = 0.526033
* conserved omega: 0.139216
* foreground omega: 15.093704