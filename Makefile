clean:
	find . -name *obtained-* | xargs rm -f
	find . -name token*.dot | xargs rm -f
	find . -name ignore-* | xargs rm -rf
	find . -name history.txt | xargs rm -rf
	find . -name *.raxml.* | xargs rm -rf
	find . -name *.ckp.gz | xargs rm -rf
	find . -name *.log | xargs rm -rf
	find . -name *.iqtree | xargs rm -rf
	find . -name *.treefile | xargs rm -rf
	find . -name *_phyml_* | xargs rm -rf

moreclean: clean
	find . -name '*~' | xargs rm -f
