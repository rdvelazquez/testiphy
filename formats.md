Input formats
-------------

PAML
----
For codeml and baseml, parameters are represented internally
as a list of floating-point numbers in a fixed order.  Branch
lengths come first, then kappa, then omega, then (I think) other
parameters.

In order to set parameters such as the nucleotide frequencies
in an F1x4 model of codon frequencies, we must determine
the order of the parameters in the parameter list.  We can then
specify initial values for the parameters by putting them in
a file called `in.codeml` or `in.baseml` as a space-separated
list of floating-point numbers.

One way to do this is to run a maximization analysis where you
know what the values of the parameters are, and the values are
distinguishable.  After the ML terminates, you can find the
index for your parameter by finding the index for its value in
the list of values.

When using `in.codeml` or `in.baseml` files, please include a
note (in a README.md or some other file) that indicates the
the order of the parameters for the model in question.
I suppose if the `in.codeml` files allow comments, that would
be a good place to write the parameter names.
